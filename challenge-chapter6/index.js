// Import modul express
const express = require("express");
const app = express();
const {User_game} = require("./models");

// Menggunakan JSON
app.use(express.json());

// =============== Rest API menggunakan JSON =================
// Get all user_game
app.get("/user_games", (req, res) => {
  User_game.findAll().then((user_game) => {
    res.status(200).json(user_game);
  });
});

// Get user_game by id
app.get("/user_games/:id", (req, res) => {
  User_game.findOne({
    where: {id: req.params.id},
  }).then((user_game) => {
    res.status(200).json(user_game);
  });
});

// Post an user_game
app.post("/user_games", (req, res) => {
  User_game.create({
    username: req.body.username,
    password: req.body.password,
  }).then((user_game) => {
    res.status(201).json(user_game);
  }).catch((err) => {
    res.status(422).json("Can't create an user_game")
  });
});

// Update an user_game
app.put("/user_games/:id", (req, res) => {
  User_game.update({
    username: req.body.username,
    password: req.body.password,
  },
  {
    where: {id: req.params.id},
  }).then((user_game) => {
    res.status(201).json(user_game);
  }).catch((err) => {
    res.status(422).json("Can't update an user_game")
  });
})

// Delete an article
app.delete("/user_games/:id", (req, res) => {
  User_game.destroy({
    where: {id: req.params.id},
  }).then((user_game) => {
    res.status(200).json({
      message: `User game dgn id ${req.params.id} sudah dihapus`,
    });
  });
});
// ===========================================================


// Menyalakan server
app.listen(3000, () => {
  console.log("Server running at port 3000");
});





