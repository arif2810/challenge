// Import module express
const express = require("express");
const path = require('path');
const app = express();
const {User_game} = require("./models");

// app.set('views',path.join(__dirname,'views'));
app.set("view engine", "ejs");
app.use(express.urlencoded({
  extended: false,
}));

app.use('/assets',express.static(path.join(__dirname, 'assets')));

// Menampilkan halaman Login
app.get("/", (req, res) => {
  res.render("auth/login");
});

// Proses Login statis
app.post("/login", (req, res) => {
  const {username, password} = req.body;
  if(username == "arif" && password == "123"){
    res.redirect("/home");
  }
  else{
    res.redirect("/");
  }
})

// Get all User Game
app.get("/home", (req, res) => {
  User_game.findAll().then((user_game) => {
    res.render("user_game", {
      user_game,
    });
  });
});

// Menampilkan form tambah user
app.get("/user_game/create", (req, res) => {
  res.render("user_game/create");
});

// Post tambah user Game
app.post("/user_game", (req, res) => {
  User_game.create({
    username: req.body.username,
    password: req.body.password,
  }).then((user_game) => {
    res.render("user_game/created");
  });
});

// Menampilkan form edit user
app.get('/user_game/edit/(:id)', (req, res) => {
  User_game.findOne({
    where: {id: req.params.id},
  }).then((user_game) => {
    res.render("user_game/edit", {
      id: user_game.id,
      username: user_game.username,
      password: user_game.password
    });
  });
});

// Update an user_game
app.post("/user_game/update/(:id)", (req, res) => {
  User_game.update({
    username: req.body.username,
    password: req.body.password,
  },
  {
    where: {id: req.params.id},
  }).then((user_game) => {
    res.render("user_game/updated");
  }).catch((err) => {
    res.status(422).json("Can't update an user_game")
  });
})

// Delete an user
app.get('/user_game/delete/(:id)', (req, res) => {
    User_game.destroy({
      where: {id: req.params.id},
    }).then((user_game) => {
      res.render("user_game/deleted");
    });
});

// app.route('/user_game/delete/(:id)')
//   .get(async function (req, res) {
//       User_game.destroy({
//         where: {id:  req.params.id},
//       }).then((user_game) => {
//         res.send("Data berhasil dihapus");
//       });
//   });


// Menyalakan server
const port = 8000
app.listen(port, () => {
  console.log(`Server running at port ${port}`);
});