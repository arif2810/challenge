// Implementasi in Express
// npm install ejs
const express = require("express");
const app = express();
const { Articles } = require("./models");

app.use(express.urlencoded({ extended: false }));

app.set("view engine", "ejs");

// POST article
app.post("/articles", (req, res) => {
  Articles.create({
    title: req.body.title,
    author: req.body.author,
    body: req.body.body,
  }).then((articles) => {
    res.send("Artikel berhasil dibuat");
  });
});

// Get message success
app.get("/articles/create", (req, res) => {
  res.render("articles/create");
});

// Get all articles
app.get("/articles", (req, res) => {
  Articles.findAll().then((articles) => {
    res.render("articles/index", {
      articles,
    });
  });
});

// run: http://localhost:3000/articles

app.listen(3000, () => {
  console.log("Server running at port 3000");
});

// run http://localhost:3000/articles/create
